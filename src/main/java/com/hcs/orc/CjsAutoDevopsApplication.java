package com.hcs.orc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CjsAutoDevopsApplication {

	public static void main(String[] args) {
		SpringApplication.run(CjsAutoDevopsApplication.class, args);
	}

}
