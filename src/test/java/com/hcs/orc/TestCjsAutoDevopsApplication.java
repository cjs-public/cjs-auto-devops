package com.hcs.orc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.test.context.TestConfiguration;

@TestConfiguration(proxyBeanMethods = false)
public class TestCjsAutoDevopsApplication {

	public static void main(String[] args) {
		SpringApplication.from(CjsAutoDevopsApplication::main).with(TestCjsAutoDevopsApplication.class).run(args);
	}

}
